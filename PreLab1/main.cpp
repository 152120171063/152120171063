/**
* @author:	Pinar Kizilarslan --> e-mail: pinarkzlarslan@gmail.com
* @date	 :	6 Ekim 2019 Pazar
* @brief :	Bu kod parcacigi input.txt dosyasindan sayilar ceker; bunlari toplar, carpar ve en kucugu bulur.
*/
#include<iostream>
#include<fstream>
#define max 3
using namespace std;
void sum();
void product();
void smallest();

int main()
{
	sum();
	product();
	smallest();

	system("pause");
}

/**
* @brief	 :	Bu fonksiyon input.txt dosyasindan okunan sayilari toplar ve sonucu yazdirir.
* @see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void sum() {
	fstream dataFile;
	dataFile.open("input.txt", ios::in);
	int numbers[max], i, sum = 0;

	for (i = 0; i < max; i++)
	{
		dataFile >> numbers[i];
		sum += numbers[i];
	}
	cout << "Sum is: " << sum << endl;

	dataFile.close();
}

/**
* @brief	 :	Bu fonksiyon input.txt dosyasindan okunan sayilari carpar ve sonucu yazdirir.
* @see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void product() {
	fstream dataFile;
	dataFile.open("input.txt", ios::in);
	int numbers[max], i, product = 1;

	for (i = 0; i < max; i++)
    {
		dataFile >> numbers[i];
		product *= numbers[i];
	}
	cout << "Product is: " << product << endl;

	dataFile.close();
}

/**
* @brief	 :	Bu fonksiyon input.txt dosyasindan okunan sayilari karsilastirip en kucugu bulur ve yazdirir.
* @see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void smallest()
{
	fstream dataFile;
	dataFile.open("input.txt", ios::in);
	int numbers[max], i;
	for (i = 0; i < max; i++)
		dataFile >> numbers[i];

	int smallest = numbers[0];
	for (i = 1; i < max; i++)
	{
		if (smallest > numbers[i])
			smallest = numbers[i];
	}
	cout << "Smallest is: " << smallest << endl;

	dataFile.close();
}