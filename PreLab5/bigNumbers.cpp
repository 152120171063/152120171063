/**
 * bigNumbers.cpp

 *  Created on: 5 Kas 2019
 *      Author: pinarkzlarslan
 */
#include "bigNumbers.h"
using namespace std;

//Multiplication
/**
* @brief :	Bu fonksiyon, ilk de�er atamas� yapar.
*/
Multiplication::Multiplication()
{
	x = 0;
	y = 0;
	result = 0;
}
/**
* @brief :	Bu fonksiyon y�k�c� fonksiyondur.
*/
Multiplication::~Multiplication(){

}
/**
* @brief :	Bu fonksiyon, dosyadan okutulan b�y�k say�lar� �arpar.
* @param str :	Input olarak al�nan, kulland���m�z de�i�kendir.
* @param str2 :	Input olarak al�nan, kulland���m�z de�i�kendir.

*/
void hesaplama::doMultiplication(string str, string str2) {

	string a, a2;
	int strtoint = 0, strtoint2 = 0, elde = 0, buyukluk = 0, kucukluk = 0, toplam = 0, fark = 0, sayaccol = 0, sayacrow = 0, carpmak = 0, toplamak = 0;;
	int **dizi;
	int *dizi2;
	string kucukstr, buyukstr;
	if (str.length() <= str2.length()){
		buyukluk = str2.length();
		kucukluk = str.length();
		buyukstr = str2;
		kucukstr = str;
	}
	else{
		buyukluk = str.length();
		kucukluk = str2.length();
		buyukstr = str;
		kucukstr = str2;
	}
	toplam = kucukluk + buyukluk + 1;

	dizi = new int*[kucukluk];
	for (int i = 0; i < kucukluk; i++)	
		dizi[i] = new int[toplam];
	
	
	toplam = kucukluk + buyukluk + 1;
	sayaccol = toplam;
	elde = 0;
	for (int i = kucukluk - 1; i >= 0; i--){
		a = kucukstr[i];
		strtoint = stoi(a);
		sayaccol = toplam;
		carpmak = 0;
		for (int j = buyukluk - 1; j >= 0; j--){
			carpmak = 0;
			a2 = buyukstr[j];
			strtoint2 = stoi(a2);

			carpmak += strtoint * strtoint2 + elde;
			elde = carpmak / 10;
			carpmak = carpmak % 10;
			dizi[sayacrow][sayaccol - sayacrow - 1] = carpmak;
			sayaccol--;
		}
		if (elde > 0)		
			dizi[sayacrow][sayaccol - sayacrow - 1] = elde;
		
		sayacrow++;
		elde = 0;
	}

	for (int i = 0; i < kucukluk; i++)
	{
		cout << endl;
		for (int j = 0; j < toplam; j++){
			if (dizi[i][j] < 0)			
				dizi[i][j] = 0;			
		}
	}
	dizi2 = new int[toplam];
	for (int i = 0; i < toplam; i++)	
		dizi2[i] = 0;
	
	cout << endl;
	for (int i = toplam - 1; i >= 0; i--)
	{
		elde = 0;
		toplamak = 0;
		for (int j = 0; j < kucukluk; j++)		
			toplamak += dizi[j][i];
		
		elde = toplamak / 10;
		toplamak = toplamak % 10;
		dizi2[i] += toplamak;
		if (elde > 0)		
			dizi2[i - 1] = elde;
		
		if (dizi2[i] > 9){
			dizi2[i] = dizi2[i] - 10;
			dizi2[i - 1] = dizi2[i - 1] + 1;
		}
	}
	cout << "Carpma Islemi Sonucu: ";
	for (int i = 0; i < toplam; i++)	
		cout << dizi2[i] << " ";
}

//Addition
/**
* @brief :	Bu fonksiyon, ilk de�er atamas� yapar.
*/
Addition::Addition()
{
	x = 0;
	y = 0;
	result = 0;
}
/**
* @brief :	Bu fonksiyon y�k�c� fonksiyondur.
*/
Addition::~Addition(){

}
/**
* @brief :	Bu fonksiyon, dosyadan okutulan b�y�k say�lar� toplar.
* @param str :	Input olarak al�nan, kulland���m�z de�i�kendir.
* @param str2 :	Input olarak al�nan, kulland���m�z de�i�kendir.

*/
void hesaplama::doAddition(string str, string str2) {

	string a, a2;
	int  elde = 0, buyukluk = 0, kucukluk = 0, fark = 0;
	int *dizi = 0;
	string kucukstr, buyukstr;
	if (str.length() <= str2.length())
	{
		buyukluk = str2.length();
		kucukluk = str.length();
		buyukstr = str2;
		kucukstr = str;
	}
	else
	{
		buyukluk = str.length();
		kucukluk = str2.length();
		buyukstr = str;
		kucukstr = str2;
	}
	fark = buyukluk - kucukluk;
	buyukluk++;


	dizi = new int[buyukluk];
	for (int i = 0; i < buyukluk; i++)	
		dizi[i] = 0;
	
	for (int i = buyukluk - 2; i >= 0; i--)
	{
		a = buyukstr[i];
		if (i - fark < 0)		
			a2 = "0";
		
		else		
			a2 = kucukstr[i - fark];
		
		dizi[i + 1] += stoi(a);
		dizi[i + 1] += stoi(a2);
		elde = 0;
		if (dizi[i + 1] > 9)
		{
			dizi[i + 1] = dizi[i + 1] - 10;
			dizi[i] = dizi[i] + 1;
		}
	}
	cout << "Toplama Islemi Sonucu: ";
	for (int i = 1; i < buyukluk; i++)
		cout << dizi[i] << " ";	
}

//Subtraction
/**
* @brief :	Bu fonksiyon, ilk de�er atamas� yapar.
*/
Subtraction::Subtraction()
{
	x = 0;
	y = 0;
	result = 0;
}
/**
* @brief :	Bu fonksiyon y�k�c� fonksiyondur.
*/
Subtraction::~Subtraction(){

}
/**
* @brief :	Bu fonksiyon, dosyadan okutulan b�y�k say�lar� birbirinden ��kar�r.
* @param str :	Input olarak al�nan, kulland���m�z de�i�kendir.
* @param str2 :	Input olarak al�nan, kulland���m�z de�i�kendir.

*/
void hesaplama::doSubtraction(string str, string str2) {
	string a, a2;
	int  elde = 0, buyukluk = 0, kucukluk = 0, fark = 0, sayac = 0, sayac2 = 0;
	int *dizi = 0;
	string kucukstr, buyukstr;
	if (str.length() < str2.length())
	{
		buyukluk = str2.length();
		kucukluk = str.length();
		buyukstr = str2;
		kucukstr = str;
	}
	else
	{
		buyukluk = str.length();
		kucukluk = str2.length();
		buyukstr = str;
		kucukstr = str2;
	}
	dizi = new int[buyukluk];
	for (int i = 0; i < buyukluk; i++)	
		dizi[i] = 0;
	
	fark = buyukluk - kucukluk;
	cout << endl << endl << endl;
	for (int j = buyukluk - 1; j >= 0; j--)
	{
		a = buyukstr[j];
		dizi[j] = stoi(a);
	}
	for (int i = buyukluk - 1; i >= 0; i--)
	{
		if (i - fark < 0)		
			a2 = "0";
		
		else
			a2 = kucukstr[i - fark];	

		dizi[i] -= stoi(a2);
		sayac = 0;
		if (dizi[i] < 0)
		{
			for (int j = i; j > 0; j--)
			{
				sayac++;
				if (dizi[j - 1] > 0)
				{
					dizi[j - 1] --;
					sayac2 = j;
					break;
				}
			}
			if (sayac > 1)
			{
				for (int k = 1; k < sayac; k++)			
					dizi[sayac2 + k - 1] = 9;	
			}
			dizi[i] = dizi[i] + 10;
		}
	}
	cout << "Cikarma Islemi Sonucu: ";
	if (str2.length() > str.length())	
		cout << " - ";
	
	for (int i = 0; i < buyukluk; i++)	
		cout << dizi[i] << " ";	
}