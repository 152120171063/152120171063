#include "bigNumbers.h"
/**
* @author:	P�nar K�z�larslan --> e-mail: pinarkzlarslan@gmail.com
* @date  :	5 Kas�m 2019 Sal�
* @brief :	Bu kod par�ac��� b�y�k haneli say�larda toplama, ��karma ve �arpma i�lemi yap�lmas� i�in yaz�lm��t�r
*/
int main()
{
	hesaplama hesapmak;
	ifstream dosya;
	string sayisayisi, sayiuzunluk1, sayiuzunluk2, sayiuzunluk3, islem, islem2 = "0", gecici;
	int sayisi, hangisayi1, hangisayi2, sayac = 1;

	{
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		dosya >> sayisayisi;
		sayisi = stoi(sayisayisi);
		while (1)
		{
			dosya >> islem;
			if (islem == "A"){
				sayac++;
				break;
			}
			sayac++;
		}
		dosya >> hangisayi1;
		dosya >> hangisayi2;
		dosya.close();
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		for (int i = 0; i < (sayisi * 2) + 1; i++)
		{
			dosya >> hesapmak.longsayi;
			if (i == (hangisayi1 * 2))			
				break;	
		}
		dosya.close();
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		for (int i = 0; i < (sayisi * 2) + 1; i++)
		{
			dosya >> hesapmak.longsayi2;
			if (i == (hangisayi2 * 2))			
				break;	
		}
		dosya.close();

		if (islem == "A")		
			hesapmak.doAddition(hesapmak.longsayi, hesapmak.longsayi2);		
	}	
	{
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		dosya >> sayisayisi;
		sayisi = stoi(sayisayisi);
		while (1)
		{
			dosya >> islem;
			if (islem == "M"){
				sayac++;
				break;
			}
			sayac++;
		}
		dosya >> hangisayi1;
		dosya >> hangisayi2;
		dosya.close();
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		for (int i = 0; i < (sayisi * 2) + 1; i++){
			dosya >> hesapmak.longsayi;
			if (i == (hangisayi1 * 2))			
				break;	
		}
		dosya.close();
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		for (int i = 0; i < (sayisi * 2) + 1; i++)
		{
			dosya >> hesapmak.longsayi2;
			if (i == (hangisayi2 * 2))			
				break;	
		}
		dosya.close();
		if (islem == "M")	
			hesapmak.doMultiplication(hesapmak.longsayi, hesapmak.longsayi2);
				
	}
	{
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		dosya >> sayisayisi;
		sayisi = stoi(sayisayisi);
		while (1)
		{
			dosya >> islem;
			if (islem == "S"){
				sayac++;
				break;
			}
			sayac++;
		}
		dosya >> hangisayi1;
		dosya >> hangisayi2;
		dosya.close();
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		for (int i = 0; i < (sayisi * 2) + 1; i++)
		{
			dosya >> hesapmak.longsayi;
			if (i == (hangisayi1 * 2))			
				break;		
		}
		dosya.close();
		dosya.is_open();
		dosya.open("input.txt", ios::app);
		for (int i = 0; i < (sayisi * 2) + 1; i++)
		{
			dosya >> hesapmak.longsayi2;
			if (i == (hangisayi2 * 2))			
				break;
		}
		dosya.close();

		if (islem == "S")		
			hesapmak.doSubtraction(hesapmak.longsayi, hesapmak.longsayi2);		
	}
	cout << endl << endl << endl;

	system("pause");
}