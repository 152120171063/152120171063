/**
 * bigNumbers.h

 *  Created on: 5 Kas 2019
 *      Author: pinarkzlarslan
 */
#include<iostream>
#include<string>
#include<stdlib.h>
#include<fstream>
using namespace std;

class Multiplication {
private:
	char *x;
	char *y;
	char *result;

public:
	Multiplication();
	~Multiplication();
	void print();
};

class Addition {
private:
	char *x;
	char *y;
	char *result;

public:
	Addition();
	~Addition();
	void print();
};

class Subtraction {
private:
	char *x;
	char *y;
	char *result;

public:
	Subtraction();
	~Subtraction();
	void print();
};

class FileReader {
private:
	fstream filePath;

};

class hesaplama {
private:

public:
	void doMultiplication(string, string);
	void doAddition(string, string);
	void doSubtraction(string, string);
	string longsayi, longsayi2, longsayi3;
};