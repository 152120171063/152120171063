/**
* @author:	P�nar K�z�larslan --> e-mail: pinarkzlarslan@gmail.com
* @date  :	15 Ekim 2019 Sal�
* @brief :	Bu kod par�ac��� olu�turdu�umuz k�t�phaneden fonksiyonlar� �a��r�r ve onlar� kullan�r.
*/
#include "Matrix.h"
#include <iostream>
#include <iomanip>
using namespace std;
/**
*\brief			  :	Bu fonksiyon ile matrisin her bir elemani icin hafizada yer acilir.
*\param matrix    :	Referans olarak atanir.
*\param rowSize   :	Olusturululan matrisin satir boyutunu gosterir.
*\param columnSize:	Olusturulan matrisin sutun boyutunu gosterir.
*/
void Matrix_Allocate(Matrix& matrix, int rowSize, int columnSize)
{
	matrix.rowSize = rowSize;
	matrix.columnSize = columnSize;
	matrix.data = new float*[rowSize];
	for (int i = 0; i < rowSize; i++)
		matrix.data[i] = new float[columnSize];
}
/**
*\brief       :	Bu fonksiyon ile olusturulmus olan matris elemanlarini tek tek silinir.
*\param matrix:	Referans olarak atanir.
*/
void Matrix_Free(Matrix& matrix)
{	
	for (int i = 0; i < matrix.rowSize; i++)
		delete[] matrix.data[i];
	
	delete[] matrix.data;

	matrix.rowSize = -1;
	matrix.columnSize = -1;
	matrix.data = nullptr;
} 
/**
*\brief       :	Bu fonksiyon ile olusturulan matrisin elemanlarina deger atanir.
*\param matrix:	Referans olarak atanir.
*\param value :	Olusturulan matrise atacanak sayiyi gosterir
*/
void Matrix_FillByValue(Matrix& matrix, float value)
{
	for (int i = 0; i < matrix.rowSize; i++)
	{
		for (int j = 0; j < matrix.columnSize; j++)		
			matrix.data[i][j] = value;
	}
}
/**
*\brief       :	Bu fonksiyon ile olusturulan matrisin elemanlarina deger atanir.
*\param matrix:	Referans olarak atanir.
*\param data  :	Olusturulan matrise atacanak cift pointerli yapiyi gosterir
*/
void Matrix_FillByData(Matrix & matrix, float ** data) 
{
	for (int i = 0; i < matrix.rowSize; i++)
	{
		for (int j = 0; j < matrix.columnSize; j++)		
			matrix.data[i][j] = data[i][j];
	}
}
/**
*\brief       :	Bu fonksiyon ile olusturulan matrisin elemanlarini ekrana yazar.
*\param matrix:	Referans olarak atanir.
*/
void Matrix_Display(const Matrix& matrix) 
{ 
	cout << "MATRIX : " << matrix.rowSize << " x " << matrix.columnSize << endl;
	for (int i = 0; i < matrix.rowSize; i++)
	{
		for (int j = 0; j < matrix.columnSize; j++)		
			cout << setw(10) << matrix.data[i][j];

		cout << endl;
	}
} 
/**
*\brief             :	Bu fonksiyon ile olusturulan matrisin elemanlari toplanir ve result matrisine atanir
*\param matrix_left :	Referans olarak atanir.
*\param matrix_right:	Referans olarak atanir.
*\param result     :	Referans olarak atanir.
*/
void Matrix_Addition(const Matrix & matrix_left, const Matrix & matrix_right,   Matrix & result)
{ 
	Matrix_Allocate(result, matrix_left.rowSize, matrix_right.columnSize); //result matrisi i�in yer a�t�k
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)		
			result.data[i][j] = matrix_left.data[i][j] + matrix_right.data[i][j];		
	}
}
/**
*\brief             :	Bu fonksiyon ile Olusturulan matrisin elemanlari cikarilir ve result matrisine atanir.
*\param matrix_left :	Referans olarak atanir.
*\param matrix_right:	Referans olarak atanir.
*\param result     :	Referans olarak atanir.
*/
void Matrix_Substruction(const Matrix & matrix_left,   const Matrix & matrix_right, Matrix & result)
{
	Matrix_Allocate(result, matrix_left.rowSize, matrix_right.columnSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)		
			result.data[i][j] = matrix_left.data[i][j] - matrix_right.data[i][j];		
	}
}
/**
*\brief             :	Bu fonksiyon ile olusturulan iki matrisin elemanlari carpilir ve result matrisine atanir
*\param matrix_left :	Referans olarak atanir.
*\param matrix_right:	Referans olarak atanir.
*\param result      :	Referans olarak atanir.
*/
void Matrix_Multiplication(const Matrix & matrix_left, const Matrix & matrix_right, Matrix & result) //matris �arp�m� ??
{
	Matrix_Allocate(result, matrix_left.rowSize, matrix_right.columnSize);
	for (int i = 0; i < matrix_left.rowSize; i++)
	{
		for (int j = 0; j < matrix_right.columnSize; j++)
		{
			result.data[i][j] = 0;
			for (int k = 0; k < matrix_left.columnSize; k++)			
				result.data[i][j] = result.data[i][j] + (matrix_left.data[i][k] * matrix_right.data[k][j]);			
		}
	}
}
/**
*\brief            :	Bu fonksiyon ile olusturulan matrisin elemanlari verilen deger ile carpilir ve result matrisine atanir
*\param matrix_left:	Referans olarak atanir.
*\param scalarValue:	Sabit deger olarak atanir ve matrisin elemanlariyla carpilir.
*\param result     :	Referans olarak atanir.
*/
void Matrix_Multiplication(const Matrix & matrix_left, float scalarValue,   Matrix & result) //bi say� ile(skaler) �arp�m??
{ 
	Matrix_Allocate(result, matrix_left.rowSize, matrix_left.columnSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)		
			result.data[i][j] = matrix_left.data[i][j] * scalarValue;		
	}
}
/**
*\brief            :Bu fonksiyon ile olusturulan matrisin elemanlari verilen degere bolunur ve result matrisine atanir
*\param matrix_left:referans olarak atanir.
*\param scalarValue:sabit deger olarak atanir ve matrisin elemanlarina bolunur
*\param result     :referans olarak atanir.
*/
void Matrix_Division(const Matrix & matrix_left, float scalarValue,   Matrix & result)
{
	Matrix_Allocate(result, matrix_left.rowSize, matrix_left.columnSize);
	for (int i = 0; i < result.rowSize; i++)
	{
		for (int j = 0; j < result.columnSize; j++)		
			result.data[i][j] = matrix_left.data[i][j] / scalarValue;		
	}
}